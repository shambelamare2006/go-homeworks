package main

import (
	"bytes"
	"fmt"
	"os"
	"strings"

	"golang.org/x/net/html"
)

var raw = `<!DOCTYPE html>
<html lang="en">

<head>
   
</head>

<body>
<div>

    <div>
		<p>this is that alfa nakfow</p>
		<p>this is that this alfa nakfow</p>
		<p>this is that this alfa nakfow</p>
		<p>this is that this alfa nakfow</p>
	<section>
		<p>this is that alfa nakfow</p>
		<p>this is that this alfa nakfow</p>
		<p>this is that this alfa nakfow</p>
		<p>this is that this alfa nakfow</p>
		<img src="xxx.png" alt="first image">
		<img src="xxx.png" alt="first image">
		<p>Links:</p><ul><li><a href="foo">Foo</a><li><a href="/bar/baz">BarBaz</a></ul>
	</section>
	<section>
		<p>this is that alfa nakfow</p>
		<p>this is that this alfa nakfow</p>
		<p>this is that this alfa nakfow</p>
		<p>this is that this alfa nakfow</p>
		<img src="xxx.png" alt="first image">
		<img src="xxx.png" alt="first image">
	</section>

    </div>
    </div>

</body>

</html>`

func countWordsAndImages(doc *html.Node) (int, int, int) {
	var words, images, links int
	visit(doc, &words, &images, &links)
	return words, images, links
}
func visit(nod *html.Node, words, images, links *int) {

	if nod.Type == html.TextNode {
		*words += len(strings.Fields(nod.Data))
	} else if nod.Type == html.ElementNode && nod.Data == "img" {
		*images++
	} else if nod.Type == html.ElementNode && nod.Data == "a" {
		for _, a := range nod.Attr {
			if a.Key == "href" {
				*links++
				break
			}
		}
	}
	for i := nod.FirstChild; i != nil; i = i.NextSibling {
		visit(i, words, images, links)
	}

}
func main() {
	doc, err := html.Parse(bytes.NewReader([]byte(raw)))

	if err != nil {
		fmt.Fprintf(os.Stderr, "parse faild. %s\n", err)
		os.Exit(-1)
	}

	words, imgs, links := countWordsAndImages(doc)
	fmt.Printf("the docmunt has %d words and %d images %d links\n", words, imgs, links)
}
