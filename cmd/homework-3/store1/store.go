package main

import (
	"errors"
	"fmt"
	"net/http"
	"strconv"
	"sync"
)

type dollars float32

type database map[string]dollars

func (db database) list(w http.ResponseWriter, req *http.Request) {

	if req.Method != "GET" {
		msg := fmt.Sprintf("Bad Request Method; %s", req.Method)
		http.Error(w, msg, http.StatusMethodNotAllowed)
		return
	}
	fmt.Printf("server: %s /\n", req.Method)
	for item, price := range db {

		fmt.Fprintf(w, "%s: %v\n", item, price)
	}

}

func (db database) add(w http.ResponseWriter, req *http.Request) {

	if req.Method != "POST" {
		msg := fmt.Sprintf("Bad Request Method; %s", req.Method)
		http.Error(w, msg, http.StatusMethodNotAllowed)
		return
	}

	item := req.URL.Query().Get("item")
	price := req.URL.Query().Get("price")
	fmt.Printf("server: %s /\n", req.Method)
	if _, ok := db[item]; ok {
		msg := fmt.Sprintf("duplicate item:%q", item)
		http.Error(w, msg, http.StatusBadRequest)
		return
	}
	p, err := strconv.ParseFloat(price, 32)
	if err != nil {
		msg := fmt.Sprintf("Invalid Price :%q", price)
		http.Error(w, msg, http.StatusBadRequest)
		return
	}

	db[item] = dollars(p)
	fmt.Fprintf(w, "added %s with price %v\n", item, db[item])
}

func run(db database, serverPort int, host string, wg *sync.WaitGroup) {
	go func() {
		mux := http.NewServeMux()
		mux.HandleFunc("/list", db.list)
		mux.HandleFunc("/add", db.add)

		server := http.Server{
			Addr:    fmt.Sprintf(":%d", serverPort),
			Handler: mux,
		}
		if err := server.ListenAndServe(); err != nil {
			if !errors.Is(err, http.ErrServerClosed) {
				fmt.Printf("error runnung http server: %s\n", err)
				wg.Done()
			}
		}
	}()

}
func main() {
	db := database{
		"shoes": 20,
		"socks": 12,
	}
	var wg sync.WaitGroup
	serverPort := 8080
	host := "http://localhost"
	wg.Add(1)
	run(db, serverPort, host, &wg)
	wg.Wait()
}
