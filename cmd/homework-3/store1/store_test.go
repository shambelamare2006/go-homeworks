package main

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestStore1(t *testing.T) {
	req := httptest.NewRequest(http.MethodPost, "/list", nil)
	w := httptest.NewRecorder()
	db := database{
		"shoes": 20,
		"socks": 12,
	}
	want := "shoes: 20\nsocks: 12\n"
	db.list(w, req)
	res := w.Result()
	status := w.Result().StatusCode
	defer res.Body.Close()
	data, err := ioutil.ReadAll(res.Body)
	t.Run("Error test", func(t *testing.T) {
		assertError(t, err)

	})
	t.Run("Status test", func(t *testing.T) {
		assertStatus(t, data, status, err)

	})
	t.Run("Data test", func(t *testing.T) {

		assertData(t, data, want, err)
	})

}
func assertData(t *testing.T, data []byte, want string, err error) {
	if string(data) != want {
		t.Errorf("expected '%s' got '%s'\n", want, string(data))
	}

}
func assertError(t *testing.T, err error) {
	if err == nil {
		t.Errorf("expected error not to be nil got")
	}
}
func assertStatus(t *testing.T, data []byte, status int, err error) {
	if status != 405 {
		t.Errorf("expected error to be nil got %v", err)
	}
}
