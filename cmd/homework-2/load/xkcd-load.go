package main

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"reflect"
	"sync"
	"time"
)

func getOne(i int, wg *sync.WaitGroup) []byte {
	url := fmt.Sprintf("https://xkcd.com/%d/info.0.json", i)

	resp, err := http.Get(url)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Couldn't read: %s\n", err)
		os.Exit(-1)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		fmt.Fprintf(os.Stderr, "Skipping %d: got statuscode :%d\n", i, resp.StatusCode)
		return []byte{}

	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Couldn't read Body: %s\n", err)
		os.Exit(-1)
	}
	wg.Done()
	return body
}
func worker(jobs <-chan int, result chan<- []byte) {
	var wg sync.WaitGroup

	for job := range jobs {
		wg.Add(1)
		res := getOne(job, &wg)
		result <- res
	}
	wg.Wait()
}
func GetListOfComics() {
	var (
		output io.WriteCloser = os.Stdout
		err    error
		count  int
		data   []byte
		open   bool
	)
	if len(os.Args) > 1 {
		output, err = os.Create(os.Args[1])

		if err != nil {
			fmt.Fprintln(os.Stderr, err)
			os.Exit(-1)
		}
		defer output.Close()
	}
	// the out put will be in the json array form
	// so we add brackets before and after the output

	fmt.Fprintf(output, "[")

	defer fmt.Fprintf(output, "]")
	//stop if we gat 2 404 fails
	result := make(chan []byte, 2642)
	jobs := make(chan int, 2642)
	for i := 0; i < 100; i++ {
		go worker(jobs, result)
	}

	for i := 1; i < 2643; i++ {
		jobs <- i
	}
	close(jobs)
	start := time.Now()
	for i := 1; i < 2643; i++ {
		data, open = <-result
		if reflect.DeepEqual(data, []byte{}) {
			continue
		}
		if !open {
			break
		}
		if data != nil {
			_, err = io.Copy(output, bytes.NewBuffer(data))

			fmt.Fprint(output, ",") // off by one
		}

		if err != nil {
			fmt.Fprintf(os.Stderr, "Stopped:%s\n", err)
			os.Exit(-1)
		}
		count++
	}

	fmt.Fprintf(os.Stderr, "Read %d comics\n", count)
	finished := time.Since(start)
	fmt.Printf("Finished @:%v\n", finished.Seconds())
}
func main() {
	GetListOfComics()
}
